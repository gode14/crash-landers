﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmBuilding : MonoBehaviour {

    List<Transform> swarmTiles = new List<Transform>();
    List<Transform> newSwarmTiles = new List<Transform>();

    public GameObject swarmMarkerDefault;

    struct SwarmMarker
    {
        public TileInfo ti;
        public GameObject marker;

        public SwarmMarker(TileInfo ti, GameObject marker)
        {
            this.ti = ti;
            this.marker = marker;
        }
    }

    List<SwarmMarker> swarmMarkers = new List<SwarmMarker>();

    private void Awake()
    {
        swarmTiles.Add(transform.parent);
    }

    //spread swarm every 500 ticks
    int timer = 0;
    int refreshRate = 100;
    void FixedUpdate()
    {
        if (timer == 0)
        {
            print("spread");
            SwarmWave(); //start a wave from the swarm building position
            timer = refreshRate;
        }
        else
            timer--;
    }

    void SwarmWave()
    {
        int i = 0;
        while(i < swarmTiles.Count) // each swarm tile (starts with this building's parent included)
        {
            Spread(swarmTiles[i].position); //check all the tiles around it and if they're not swarm set them to swarm
            i++;
        }

        //some stuff to make the list of swarm tiles work
        foreach (var tile in newSwarmTiles) //each new swarm tile we made this wave
        {
            swarmTiles.Add(tile); //add it to the master list of swarm tiles
        }
        newSwarmTiles.Clear(); //clear the new swarm tile we made this wave list
    }

    void Spread(Vector3 pos)
    {
        //grab each tile within 10 uu
        Collider[] cols = Physics.OverlapSphere(pos, 10);
        foreach (var col in cols)
        {
            if (col.tag == "Tile")
            {
                TileInfo ti = col.GetComponent<TileInfo>();
                if (!ti.swarm && !ti.blocked) //if the tile info marks it a non-swarm tile
                {
                    ti.swarm = true; //make it a swarm tile
                    newSwarmTiles.Add(col.transform); //add it to the list of new swarm tiles we made this wave
                    MakeSwarmMarker(ti); //make swarm marker
                }
            }
        }
    }

    //make a spaceblob gameobject (for now)
    void MakeSwarmMarker(TileInfo tile)
    {
        GameObject marker = Instantiate(swarmMarkerDefault);
        marker.transform.position = tile.transform.position;
        marker.transform.rotation = tile.transform.rotation;
        SwarmMarker newMarker = new SwarmMarker(tile, marker);
        swarmMarkers.Add(newMarker); //swarm markers hold the visual effect gameobject and the swarm tile it's attached to
    }
}
