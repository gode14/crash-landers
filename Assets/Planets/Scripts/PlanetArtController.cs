﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlanetArtController : MonoBehaviour {

    public Material tileMat;

    public Color defaultColor;
    public Color blockedColor;
    public Color oreColor;

    public Mesh defaultTile;
    public Mesh[] blockedTiles = new Mesh[0];
    public Mesh[] oreTiles = new Mesh[0];

    public void SetColors()
    {
        //set level-specific material colors to public values in inspector

        tileMat.SetColor("_DefaultColor", defaultColor);
        tileMat.SetColor("_OreColor", oreColor);
        tileMat.SetColor("_BlockedColor", blockedColor);
    }
}
