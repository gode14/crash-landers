﻿using System.Collections.Generic;
using UnityEngine;

public class PlanetTilePlacer : MonoBehaviour
{
    Mesh tileMesh;
    public List<GameObject> tileList = new List<GameObject>();

    public void CreateTiles()
    {
        //when this button is pressed, create a tile at every vertex position of the current mesh in attached meshfilter
        tileList.Clear();

        Vector3[] tileCoords = GetComponent<MeshFilter>().sharedMesh.vertices;

        tileMesh = GetComponent<PlanetArtController>().defaultTile;
        Material mat = GetComponent<PlanetArtController>().tileMat;

        int i = 0;
        while (i < tileCoords.Length)
        {
            //create basically a prefab
            GameObject nTile = new GameObject("Tile", typeof(MeshFilter), typeof(MeshRenderer));

            nTile.transform.parent = transform;
            nTile.transform.localPosition = tileCoords[i];

            nTile.transform.LookAt(nTile.transform.position * 2 - transform.position);

            nTile.GetComponent<MeshRenderer>().sharedMaterial = mat;
            nTile.GetComponent<MeshFilter>().sharedMesh = tileMesh;

            nTile.AddComponent<MeshCollider>();
            nTile.GetComponent<MeshCollider>().convex = true;

            nTile.AddComponent<TileEditor>();
            nTile.GetComponent<TileEditor>().UpdateTile();

            nTile.AddComponent<TileInfo>();

            nTile.layer = 9;

            nTile.transform.tag = "Tile";

            nTile.isStatic = true;

            tileList.Add(nTile);

            i++;
        }
    }

    public void UpdateTiles()
    {
        CreateInstance();

        //get colors from pac
        PlanetArtController pac = FindObjectOfType<PlanetArtController>();
        pac.SetColors();

        // create new colors array where the colors will be created.
        Color[] colors = new Color[mesh.vertices.Length];

        int i = 0;
        while (i < tileList.Count)
        {
            TileEditor te = tileList[i].GetComponent<TileEditor>();
            //update all tiles
            te.UpdateTile();

            //color planet
            switch (te.type)
            {
                case TileEditor.TileType.Default:
                    colors[i] = pac.defaultColor;
                    break;

                case TileEditor.TileType.Ore:
                    colors[i] = pac.oreColor;
                    break;

                case TileEditor.TileType.Blocked:
                    colors[i] = pac.blockedColor;
                    break;

                default:
                    break;
            }
            i++;
        }

        mesh.colors = colors;
    }

    Mesh mesh;

    void CreateInstance()
    {
        //do some stuff to allow us to modify meshes in the editor
        mesh = GetComponent<MeshFilter>().sharedMesh;
        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh meshCopy = Mesh.Instantiate(mesh) as Mesh;
        mesh = mf.mesh = meshCopy;
    }
}