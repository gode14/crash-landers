﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetOrbiter : MonoBehaviour {
    
    public float orbitSpeed;
    GameObject sun;

    private void Awake()
    {
        sun = GameObject.Find("Sun");
    }

    void FixedUpdate () {
        //in this game the sun rotates around the planet because our meshes need to be static for batching
        sun.transform.RotateAround(transform.position, Vector3.up, orbitSpeed);
    }
}
