﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TileEditor : MonoBehaviour
{
    PlanetArtController pac;
    //different types of types go in this enum
    [System.Serializable]
    public enum TileType
    {
        Default,
        Blocked,
        Ore
    }

    public TileType type;

    Mesh mesh;

    [HideInInspector]
    public int oreCount;

    void SetType()
    {
        oreCount = 0;

        //change stuff according to the current type
        if (type == TileType.Default)
        {
            transform.name = "Tile : Default";
            mesh = pac.defaultTile;
            CreateInstance();
            SetUVs.UVSet(mesh, 2, 0, 0);
        }

        if (type == TileType.Ore)
        {
            transform.name = "Tile : Ore";
            mesh = pac.oreTiles[Random.Range(0, pac.oreTiles.Length)];
            CreateInstance();
            SetUVs.UVSet(mesh, 2, 1, 0);
            transform.Rotate(Vector3.forward * Random.Range(0f, 360f));

            oreCount = 1;
        }

        if (type == TileType.Blocked)
        {
            transform.name = "Tile : Blocked";
            mesh = pac.blockedTiles[Random.Range(0, pac.blockedTiles.Length)];
            CreateInstance();
            SetUVs.UVSet(mesh, 2, 0, 1);
            transform.Rotate(Vector3.forward * Random.Range(0f, 360f));
        }

        //color variation
        SetUVs.UVSet(mesh, 0, Random.Range(.9f, 1.1f), Random.Range(.6f, .4f));

    }
    
    void CreateInstance()
    {
        //do some stuff to allow us to modify meshes in the editor
        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh meshCopy = Mesh.Instantiate(mesh) as Mesh;
        mesh = mf.mesh = meshCopy;
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    public int height;
    public int currentHeight;

    void SetHeight()
    {
        transform.position += transform.forward * (height - currentHeight) * 2;
        currentHeight = height;
    }


    public void UpdateTile()
    {
        pac = transform.root.GetComponent<PlanetArtController>();
        mesh = pac.defaultTile;
        //when we click update set height and type according to public values in inspector
        SetHeight();
        SetType();
    }
}
