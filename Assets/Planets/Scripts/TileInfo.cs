﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileInfo : MonoBehaviour
{
    //this script pulls info from tile editor on awake and is the script that should be checked for info during run time

    [HideInInspector]
    public int currentOreCount;
    public bool swarm;
    public bool blocked;
    public GameObject buildingChild;

    private void Awake()
    {
        TileEditor te = GetComponent<TileEditor>();

        if (te.type == TileEditor.TileType.Blocked)
            blocked = true;

        currentOreCount = te.oreCount;
    }
}
