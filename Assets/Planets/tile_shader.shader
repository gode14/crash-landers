// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33461,y:32970,varname:node_3138,prsc:2|emission-5064-OUT,custl-4172-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:30531,y:32505,ptovrint:False,ptlb:DefaultColor,ptin:_DefaultColor,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2d,id:2509,x:31953,y:32595,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_2509,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5310-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:5310,x:31658,y:32563,varname:node_5310,prsc:2,uv:1,uaff:False;n:type:ShaderForge.SFN_Blend,id:8438,x:32450,y:32815,varname:node_8438,prsc:2,blmd:10,clmp:True|SRC-2509-RGB,DST-3749-OUT;n:type:ShaderForge.SFN_LightVector,id:3342,x:31635,y:33373,varname:node_3342,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:5385,x:31456,y:33522,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:6237,x:31847,y:33416,cmnt:Lambert,varname:node_6237,prsc:2,dt:1|A-3342-OUT,B-5385-OUT;n:type:ShaderForge.SFN_Multiply,id:3348,x:32242,y:33412,cmnt:Diffuse Contribution,varname:node_3348,prsc:2|A-8438-OUT,B-6237-OUT;n:type:ShaderForge.SFN_Multiply,id:7997,x:32756,y:33671,cmnt:Attenuate and Color,varname:node_7997,prsc:2|A-3348-OUT,B-4862-RGB,C-5367-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:5367,x:32511,y:33805,varname:node_5367,prsc:2;n:type:ShaderForge.SFN_LightColor,id:4862,x:32511,y:33671,varname:node_4862,prsc:2;n:type:ShaderForge.SFN_Desaturate,id:3749,x:32263,y:32858,varname:node_3749,prsc:2|COL-368-OUT,DES-2810-OUT;n:type:ShaderForge.SFN_TexCoord,id:4693,x:31514,y:33113,varname:node_4693,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:368,x:31889,y:32814,varname:node_368,prsc:2|A-4183-OUT,B-4693-U;n:type:ShaderForge.SFN_Add,id:2810,x:31924,y:33085,varname:node_2810,prsc:2|A-4693-V,B-9811-OUT;n:type:ShaderForge.SFN_Vector1,id:9811,x:31720,y:33218,varname:node_9811,prsc:2,v1:-0.5;n:type:ShaderForge.SFN_Multiply,id:5064,x:32733,y:32787,varname:node_5064,prsc:2|A-8438-OUT,B-2849-OUT;n:type:ShaderForge.SFN_Vector1,id:2849,x:32522,y:33022,varname:node_2849,prsc:2,v1:0.4;n:type:ShaderForge.SFN_TexCoord,id:6104,x:30583,y:32862,varname:node_6104,prsc:2,uv:2,uaff:False;n:type:ShaderForge.SFN_Lerp,id:8971,x:30898,y:32747,varname:node_8971,prsc:2|A-7241-RGB,B-2471-RGB,T-6104-U;n:type:ShaderForge.SFN_Color,id:2471,x:30430,y:32679,ptovrint:False,ptlb:OreColor,ptin:_OreColor,varname:node_2471,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:4183,x:31200,y:32794,varname:node_4183,prsc:2|A-8971-OUT,B-6971-RGB,T-6104-V;n:type:ShaderForge.SFN_Color,id:6971,x:30876,y:33037,ptovrint:False,ptlb:BlockedColor,ptin:_BlockedColor,varname:_OreColor_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:4172,x:33212,y:33500,varname:node_4172,prsc:2|A-7997-OUT,B-4863-OUT;n:type:ShaderForge.SFN_Vector1,id:4863,x:32969,y:33665,varname:node_4863,prsc:2,v1:2;proporder:7241-2509-2471-6971;pass:END;sub:END;*/

Shader "Custom/Tile Shader" {
    Properties {
        _DefaultColor ("DefaultColor", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Diffuse ("Diffuse", 2D) = "white" {}
        _OreColor ("OreColor", Color) = (0.5,0.5,0.5,1)
        _BlockedColor ("BlockedColor", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _DefaultColor;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _OreColor;
            uniform float4 _BlockedColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv1, _Diffuse));
                float3 node_8438 = saturate(( lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5))) > 0.5 ? (1.0-(1.0-2.0*(lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5)))-0.5))*(1.0-_Diffuse_var.rgb)) : (2.0*lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5)))*_Diffuse_var.rgb) ));
                float3 emissive = (node_8438*0.4);
                float3 finalColor = emissive + (((node_8438*max(0,dot(lightDirection,normalDirection)))*_LightColor0.rgb*attenuation)*2.0);
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _DefaultColor;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _OreColor;
            uniform float4 _BlockedColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv1, _Diffuse));
                float3 node_8438 = saturate(( lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5))) > 0.5 ? (1.0-(1.0-2.0*(lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5)))-0.5))*(1.0-_Diffuse_var.rgb)) : (2.0*lerp((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),dot((lerp(lerp(_DefaultColor.rgb,_OreColor.rgb,i.uv2.r),_BlockedColor.rgb,i.uv2.g)*i.uv0.r),float3(0.3,0.59,0.11)),(i.uv0.g+(-0.5)))*_Diffuse_var.rgb) ));
                float3 finalColor = (((node_8438*max(0,dot(lightDirection,normalDirection)))*_LightColor0.rgb*attenuation)*2.0);
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
