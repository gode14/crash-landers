﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(TileEditor))]
[CanEditMultipleObjects]
public class InspectorTileEditor : Editor
{
    SerializedProperty height;
    SerializedProperty type;

    private void OnEnable()
    {

        height = serializedObject.FindProperty("height");
        type = serializedObject.FindProperty("type");
    }

    
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(height);
        EditorGUILayout.PropertyField(type);

        if (GUILayout.Button("Update Tiles"))
            FindObjectOfType<PlanetTilePlacer>().UpdateTiles();

        serializedObject.ApplyModifiedProperties();
    }
}
