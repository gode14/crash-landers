﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(PlanetTilePlacer))]
public class EPlanetTilePlacer : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PlanetTilePlacer placer = target as PlanetTilePlacer;

        if (GUILayout.Button("Create Tiles"))
            placer.CreateTiles();
        
        if (GUILayout.Button("Update Tiles"))
            placer.UpdateTiles();
    }
}