﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreDeployer : MonoBehaviour
{

    public GameObject buildingCore;
    ObjectSelector objSel;
    bool deployTileChosen = false;

    private void Awake()
    {
        objSel = GetComponent<ObjectSelector>();
    }

    private void FixedUpdate()
    {
        //Destroy this script and snap the lander if we're within .02 uu
        if (deployTileChosen)
        {
            if ((core.transform.position - core.transform.parent.position).magnitude < 0.02f)
            {
                core.transform.position = core.transform.parent.position;
                Destroy(this);
            }
        }

        //if we havent chosen yet and we choose, run deploy code
        if (objSel.selectedObj != null && !deployTileChosen)
        {
            deployTileChosen = true;
            Deploy(objSel.selectedObj);
        }
    }

    private void Update()
    {
        //if we've chosen tile, move the lander
        if (deployTileChosen)
            core.transform.position = Vector3.Lerp(core.transform.position, core.transform.parent.position, 2f * Time.deltaTime);
    }

    GameObject core;
    void Deploy(Transform deployTile)
    {
        //make the lander
        core = Instantiate(buildingCore, Vector3.zero, Quaternion.identity);
        core.transform.rotation = deployTile.rotation;
        core.transform.position = deployTile.position + deployTile.forward * 200;
        core.transform.parent = deployTile;

        //let deployTile know that it has a building on it now (normally in BuildManager)
        deployTile.GetComponent<TileInfo>().buildingChild = core;

        //set starting resources
        GetComponent<ResourceManager>().mineralTotal = 50;

        //enable building button
        //GameObject.Find("BuildingButton").GetComponent<Button>().interactable = true;

        //destroy deploy command
        Destroy(GameObject.Find("DeployMessage"));
    }
}
