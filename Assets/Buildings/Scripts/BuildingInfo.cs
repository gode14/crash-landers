﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingInfo : MonoBehaviour {

    //this scripts stores common info that all buildings have

    [HideInInspector]
    public TileInfo parentTile; //premanent reference to our parent tile

    [Header("HealthInfo")]
    public int maxHealth; //max health
    public int currentHealth; //current health

    [Header("CostInfo")]
    public int mineralCost; //mineral cost to build
    public int energyCost; //energy cost to draw from ResourceManager
    [HideInInspector]
    public int energyCostCurrent; //energy cost after modifiers (0 if buildings inactive)

    [HideInInspector]
    public bool electicLit; //if this building is near a light building (controller by DarknessManager)
    [HideInInspector]
    public bool sunLit; //how much sunlight this building is receiving (controller by DarknessManager) NOTE: this is just 1 or 0 for now, that may change

    [Space(10)]
    [Header("ActiveInfo")]
    public int crew; //the number of crew in this building (buildings use this number to calculate things like resource gather rate)
    public bool active; //whether the building operates and draws power, controlled by player input

    //this function is called by player input to toggle active state
    public void toggleActive()
    {
        active = !active;
        
        //if we are active, energy cost as normal, otherwise, no energy cost
        if (active)
            energyCostCurrent = energyCost;
        else
            energyCostCurrent = 0;
    }

    private void Awake()
    {
        currentHealth = maxHealth;
    }

    //called by other stuff for damage or healing
    public void ModifyHealth(int healthChange)
    {
        currentHealth += healthChange;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);

        if (currentHealth == 0)
            DestroyBuilding();
    }

    void DestroyBuilding()
    {
        parentTile.buildingChild = null;
        Destroy(gameObject);
    }
}
