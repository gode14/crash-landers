﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingLight : MonoBehaviour {

    BuildingInfo bInfo; //all buildings have a building info that stores important info like crew count and active state
    int defaultEnergyCost; //grabs the energy cost value set in the inspector so we can switch back and forth from it
    ResourceManager rm; //reference resourceman to check if we have power

    Light electricLight; //the new light that gets created
    
    private void Awake()
    {
        bInfo = GetComponent<BuildingInfo>();
        defaultEnergyCost = bInfo.energyCost;
        rm = FindObjectOfType<ResourceManager>();

        //make a gameobject that will hold our light, then parent and orient it
        GameObject lightHolder = new GameObject("LightHolder", typeof(Light));
        lightHolder.transform.parent = transform;
        lightHolder.transform.LookAt(-transform.forward);

        //set the settings for a light that covers every tile within 2 of this one
        electricLight = lightHolder.GetComponent<Light>();
        electricLight.type = LightType.Directional;
        electricLight.intensity = 0f;
        electricLight.cookieSize = 45;
        electricLight.cookie = FindObjectOfType<DarknessManager>().electricLightCookie;

        FindObjectOfType<DarknessManager>().CheckSunlight(bInfo);
    }

    private void FixedUpdate()
    {
        //can turn the light on or off to conserve power (active stored in buildinfo)
        if (bInfo.active && rm.energyTotal > 0)
        {
            electricLight.enabled = true;

            //the light shuts off automatically in sunlight but keeps a dim light for player information
            if (bInfo.sunLit)
            {
                electricLight.intensity = 0.1f;
                bInfo.energyCostCurrent = 0;
            }
            else
            {
                electricLight.intensity = 0.2f;
                bInfo.energyCostCurrent = defaultEnergyCost; //if in darkness, energy cost is set back to the value we grabbed in awake
            }

        }
        else //if inactive, disable stuff
        {
            electricLight.enabled = false;
            bInfo.energyCostCurrent = 0;
        }
    }
}
