﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BuildingManager : MonoBehaviour
{

    // An array of Building Prefabs
    public GameObject[] buildings;

    //An array to hold each building button so that we can reference their interactability. 
    public Button[] buildingButtons;

    //The UI element on the canvas that enables the building panel
    [Space(10)]
    public GameObject buildingPanelButton;

    //The Ui element on the canvas that holds the building you can build
    public GameObject buildingPanel;

    //the ghost building that appears after you click button, later turned into real building
    private GameObject currentPlaceableObject;

    private bool selectingBuilding;

    void Start()
    {
        rm = GetComponent<ResourceManager>();
        os = GetComponent<ObjectSelector>();
    }

    private void FixedUpdate()
    {
        //set which buildings you can buy based on total mineral
        SetBuildingPurchasable();
    }

    void Update()
    {
        KeyboardShortcuts();

        // if you are building and have selected a building
        if (currentPlaceableObject)
        {
            MoveCurrentObjectToHighlightedTile();

            //clicking places the object
            if (Input.GetMouseButtonDown(0))
            {
                //if we're highlighting a tile, place it and pay costs. Also tell the tile it has a building child now
                if (os.highlightedObj)
                {
                    PayResourceCost(currentPlaceableObject);
                    currentPlaceableObject.GetComponent<BuildingInfo>().toggleActive();

                    //introduce to child building and parent tile
                    os.highlightedObj.GetComponent<TileInfo>().buildingChild = currentPlaceableObject;
                    currentPlaceableObject.GetComponent<BuildingInfo>().parentTile = os.highlightedObj.GetComponent<TileInfo>();
                }
                else //Otherwise, destroy the object
                    Destroy(currentPlaceableObject);

                //either way, reset these values
                currentPlaceableObject = null;
                selectingBuilding = false;
            }
        }
    }

    void KeyboardShortcuts() //I'm just throwing EVERY input in here for now - this needs it's own InputManager
    {
        //checks for building keyboard shortcuts
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[0].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[1].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[2].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[3].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[4].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            Destroy(currentPlaceableObject);
            currentPlaceableObject = null;
            selectingBuilding = false;

            if (buildings[5].GetComponent<BuildingInfo>().mineralCost > rm.mineralTotal)
                return;

            SelectBuilding(5);
        }

        //toggle building active state if we're highlighting a tile with a building (this prob shouldnt stay here forever)
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //this is messy because only tiles can be selected rn not buildings
            if (os.highlightedObj.GetComponent<TileInfo>().buildingChild)
            {
                os.highlightedObj.GetComponent<TileInfo>().buildingChild.GetComponent<BuildingInfo>().toggleActive();
            }
        }

        //increase or decrease crew count if we're highlighting a tile with a building (this prob shouldnt stay here forever)
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //population manager needs to get involved here
            if (os.highlightedObj.GetComponent<TileInfo>().buildingChild)
                os.highlightedObj.GetComponent<TileInfo>().buildingChild.GetComponent<BuildingInfo>().crew++;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (os.highlightedObj.GetComponent<TileInfo>().buildingChild)
                os.highlightedObj.GetComponent<TileInfo>().buildingChild.GetComponent<BuildingInfo>().crew--;
        }

        //Reload scene with R
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    ResourceManager rm;

    void PayResourceCost(GameObject newBuilding)
    {
        //tell the resourceman we have a new building, subtract the mineral cost from our current minerals
        rm.mineralTotal -= newBuilding.GetComponent<BuildingInfo>().mineralCost;
        rm.UpdateUI();
    }

    public void SelectBuilding(int i)
    {
        //Building buttons calls this. They must specify an int for which building they build
        currentPlaceableObject = Instantiate(buildings[i], Vector3.zero, Quaternion.identity);
    }

    public void OpenBuildingsTab()
    {
        buildingPanel.SetActive(true);
        selectingBuilding = true;
        buildingPanelButton.SetActive(false);
    }
    public void CloseBuildingsTab()
    {
        buildingPanel.SetActive(false);
        selectingBuilding = false;
        buildingPanelButton.SetActive(true);
    }

    void SetBuildingPurchasable()
    {
        //grabs current minerals from resourceman, set building interactability based on buildingcost component in prefab
        int currentMinerals = rm.mineralTotal;

        //i was finding myself having to reassign these all the time, so this is just a check to say if nothing is assigned, don't throw an error
        if (buildingButtons.Length == 0)
            return;

        int i = 0;
        while (i < buildings.Length)
        {
            if (buildingButtons.Length > i)
            {
                if (buildings[i].GetComponent<BuildingInfo>().mineralCost <= currentMinerals)
                    buildingButtons[i].interactable = true;
                else
                    buildingButtons[i].interactable = false;
            }

            i++;
        }
    }

    ObjectSelector os;

    void MoveCurrentObjectToHighlightedTile()
    {
        //if we're not highlighting a tile, hide the currentPlaceableObject. Otherwise, parent and snap is to the tile we're highlighting 
        if (!os.highlightedObj)
        {
            currentPlaceableObject.transform.parent = null;
            currentPlaceableObject.transform.localPosition = Vector3.zero;
            return;
        }

        currentPlaceableObject.transform.parent = os.highlightedObj.transform;
        currentPlaceableObject.transform.localPosition = Vector3.zero;
        currentPlaceableObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
}
