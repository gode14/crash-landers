﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineralBuilding : MonoBehaviour {
    
    public int mineralGain; //how much mineral gain this building provides by default   
    public int mineralGainCurrent; //how much mineral gain after modifiers
    BuildingInfo bInfo; //all buildings have a building info that stores important info like crew count and active state

    private void Awake()
    {
        bInfo = GetComponent<BuildingInfo>();
    }

    private void FixedUpdate()
    {
        //if active (stored in BuildingInfo)
        if (bInfo.active)
        {
            //current mineral gain increases with more crew if there are minerals
            if (bInfo.parentTile.currentOreCount > 0)
                mineralGainCurrent = mineralGain * bInfo.crew;
        }
        else
        {
            mineralGainCurrent = 0;
        }
    }
}
