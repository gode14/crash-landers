﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBuilding : MonoBehaviour
{

    public int energyGain; //how much energy this building provides by default
    public int energyGainCurrent; //how much energy after modifiers
    public EnergyType type; //the type of energy this building provides (just used to distinguish solar right now)
    public enum EnergyType
    {
        Nuclear, //doesn't require sunlight, low power (home base)
        Solar //requires sunlight
    }
    BuildingInfo bInfo; //all buildings have a building info that stores important info like crew count and active state

    private void Awake()
    {
        bInfo = GetComponent<BuildingInfo>();
    }

    private void FixedUpdate()
    {
        //if not active (stored in BuildingInfo), do nothing
        if (bInfo.active)
        {
            //current energy gain increases with more crew
            energyGainCurrent = energyGain * bInfo.crew;

            if (type == EnergyType.Solar)
            {
                //if solar energy, only allow energy gain in sunlight
                if (!bInfo.sunLit)
                    energyGainCurrent = 0;
            }
        }
        else
        {
            energyGainCurrent = 0;
        }
    }
}
