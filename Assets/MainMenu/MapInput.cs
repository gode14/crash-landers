﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapInput : MonoBehaviour {

    public Transform levelSelector;
    Transform targetLevel = null;

    //this script checks which level we are selecting in the map and loads it if we click

    private void Update()
    {
        if (!MouseHovering())
            return;

        targetLevel = MouseHovering();

        if (Input.GetMouseButtonDown(0))
            SceneManager.LoadScene(1); //placeholder
        
        //SceneManager.LoadScene(int.Parse(MouseHovering().name)); //this loads correct scene
    }

    private void FixedUpdate()
    {
        MoveSelector();
    }

    Transform MouseHovering()
    {
        Ray ray = FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            return hit.transform;
        }

        return null;
    }

    void MoveSelector()
    {
        if (targetLevel)
            levelSelector.position = Vector3.Lerp(levelSelector.position, targetLevel.position, 0.5f);
    }
}
