﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour {


    bool viewingMap;

    public void ToggleMapScreen()
    {
        if (viewingMap)
        {
            viewingMap = false;
            targetCamRotation = 0;
            targetUIHeight = 0;
            FindObjectOfType<MapInput>().enabled = false;
        }
        else
        {
            viewingMap = true;
            targetCamRotation = -90;
            targetUIHeight = 1;
            FindObjectOfType<MapInput>().enabled = true;
        }
    }

    Camera cam;
    float targetCamRotation;
    float camRot = 0;

    RectTransform UIHolder;
    float targetUIHeight = 0;
    float heightTracker = 0;
    
    private void Awake()
    {
        cam = FindObjectOfType<Camera>();
        UIHolder = GameObject.Find("UIHolder").GetComponent<RectTransform>();
    }

    private void FixedUpdate()
    {
        camRot = EasingFunction.EaseInOutBack(camRot, targetCamRotation, 0.5f);
        cam.transform.localRotation = Quaternion.Euler(camRot, 0, 0);

        heightTracker = EasingFunction.EaseInOutCubic(heightTracker, targetUIHeight, 0.5f);
        Vector2 newPos = Vector2.Lerp(new Vector2(UIHolder.anchoredPosition.x, 0), new Vector2(UIHolder.anchoredPosition.x, -500), heightTracker);
        UIHolder.localPosition = newPos;
    }
}
