﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SetUVs {

    public static void UVSet(Mesh mesh, int UVset, float setX, float setY)
    {
        //modify the uvs of this mesh to do color variation and tile-specific colors
        Vector2[] uvs = new Vector2[mesh.vertices.Length];

        int i = 0;
        while (i < uvs.Length)
        {
            uvs[i] = new Vector2(setX, setY);
            i++;
        }

        switch (UVset)
        {
            case 0:
                mesh.uv = uvs;
                break;

            case 1:
                mesh.uv2 = uvs;
                break;

            case 2:
                mesh.uv3 = uvs;
                break;

            default:
                break;
        }
    }

}
