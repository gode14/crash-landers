﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour
{
    public int energyTotal; //tracks total energy
    public int energyGainRate; //tracks energy rate, just for player's info
    public int mineralTotal; //tracks total minerals
    public int mineralGainRate; //tracks mineral rate, just for player's info

    void Start()
    {
        //define UI components
        energyDisplay = GameObject.Find("EnergyDisplay").GetComponent<Text>();
        mineralDisplay = GameObject.Find("MineralDisplay").GetComponent<Text>();
    }

    //update resources every 50 ticks
    int timer = 0;
    int refreshRate = 50;
    void FixedUpdate()
    {
        if (timer == 0)
        {
            //energy first, so no power works properly
            UpdateEnergy();
            UpdateMinerals();
            UpdateUI();

            timer = refreshRate;
        }
        else
            timer--;
    }

    //handles energy gain and loss
    #region Energy
    void UpdateEnergy()
    {
        //totals energy based on the gain of all energy buildings and the cost of all buildings with energy cost
        energyTotal += EnergyGain();
        energyTotal -= EnergyCost();

        //caps the amount of energy we store based on sum of the capacity of all battery buildings
        energyTotal = EnergyCap(energyTotal);

        //tracking this value for player information
        energyGainRate = EnergyGain() - EnergyCost();
    }

    int EnergyGain()
    {
        //make array of all energy building components
        EnergyBuilding[] energyBuildings = FindObjectsOfType<EnergyBuilding>();

        //calculate energy gain
        int energyGain = 0;
        foreach (var eBuilding in energyBuildings)
        {
            energyGain += eBuilding.energyGainCurrent;
        }

        //add energy gain to energy total
        return energyGain;
    }

    int EnergyCost()
    {
        //make array of all energy cost components (multiple types of buildings, like lights and harvesters, use the same component for energy cost)
        BuildingInfo[] energyCostBuildings = FindObjectsOfType<BuildingInfo>();

        //calculate energy drain
        int energyCost = 0;
        foreach (var energyCostBuilding in energyCostBuildings)
        {
            energyCost += energyCostBuilding.energyCostCurrent;
        }

        //subtract energy drain from energy total
        return energyCost;
    }

    int EnergyCap(int energyTotal)
    {
        //make array of all battery building components
        BatteryBuilding[] batteryBuildings = FindObjectsOfType<BatteryBuilding>();

        //calculate energy cap
        int energyCap = 0;
        foreach (var batteryBuilding in batteryBuildings)
        {
            energyCap += batteryBuilding.capacity;
        }

        //clamp energy total so it's minimum is 0, and its maximum is energy cap
        return Mathf.Clamp(energyTotal, 0, energyCap);
    }
    #endregion

    //handles mineral gain
    #region Minerals
    void UpdateMinerals()
    {
        //if we have energy
        if (energyTotal > 0)
        {
            //updates minerals based on the gain of all mineral buildings
            mineralTotal += MineralGain();
        }
    }

    int MineralGain()
    {
        //make array of all mineral building components
        MineralBuilding[] mineralBuildings = FindObjectsOfType<MineralBuilding>();

        //calculate energy gain
        int mineralGain = 0;
        foreach (var mBuilding in mineralBuildings)
        {
            mineralGain += mBuilding.mineralGainCurrent;
        }

        //add energy gain to energy total
        return mineralGain;
    }
    #endregion

    //handles UI
    #region UI
    Text mineralDisplay;
    Text energyDisplay;

    public void UpdateUI()
    {
        mineralDisplay.text = mineralTotal + "";

        energyDisplay.text = energyTotal + "";
        if (energyGainRate >= 0)
            energyDisplay.text += " (+" + energyGainRate + ")";
        else
            energyDisplay.text += " (" + energyGainRate + ")";
    }
    #endregion
}
