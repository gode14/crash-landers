﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelector : MonoBehaviour {
    
    //highlight image
    public Sprite tileHighlightSprite;

    //the GO we move around
    GameObject tileHighlightEffect;

    //not implemented
    GameObject buildingHighlightEffect;

    //any script can access this to get the tile we're highlighting
    public Transform highlightedObj;

    //any script can access this to get the tile we've clicked on most recently, or null if we clicked on nothing
    public Transform selectedObj;

    [Space(10)]
    public LayerMask mask;

    private void Update()
    {
        //if we click, make selected obj highlighted obj, unless we click without a highlighted obj, then selected obj is null
        if (Input.GetMouseButtonDown(0))
        {
            if (highlightedObj)
                selectedObj = highlightedObj;
            else
                selectedObj = null;
        }
    }

    private void FixedUpdate()
    {
        //grab the transform of object we are raycasting in the selectable layer
        Transform mouseHovering = MouseHovering();
        
        //reset
        highlightedObj = null;

        //if we're raycasting nothing, no effect
        if (!mouseHovering)
        {
            if (tileHighlightEffect)
                Destroy(tileHighlightEffect);
            return;
        }

        //if we're raycasting a tile, make tile effect
        if (mouseHovering.tag == "Tile")
        {
            if (mouseHovering.GetComponent<TileInfo>().blocked)
                return;

            if (!tileHighlightEffect)
            {
                tileHighlightEffect = new GameObject("TileHighlight", typeof(SpriteRenderer));
                tileHighlightEffect.GetComponent<SpriteRenderer>().sprite = tileHighlightSprite;
                tileHighlightEffect.transform.localScale *= 2.5f;
            }

            tileHighlightEffect.transform.parent = mouseHovering;
            tileHighlightEffect.transform.localPosition = Vector3.forward * .1f;

            tileHighlightEffect.transform.LookAt(tileHighlightEffect.transform.position * 2 - mouseHovering.position);

            highlightedObj = mouseHovering;
        }
        else
        {
            //if raycasting a selectable obj thats not a tile (like a building) destroy the tile effect
            if (tileHighlightEffect)
                Destroy(tileHighlightEffect);
        }

        if (mouseHovering.tag == "Building")
        { 
            //not implemented, buildings have no colliders rn
            highlightedObj = mouseHovering;
        }
    }

    Transform MouseHovering()
    {
        //raycast on selectable layer

        Ray ray = FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
        {
            return hit.transform;

        }

        return null;
    }

}
