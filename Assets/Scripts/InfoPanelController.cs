﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelController : MonoBehaviour
{

    ObjectSelector ob;
    ResourceManager rm;
    Text display;

    private void Awake()
    {
        display = GetComponent<Text>();
        ob = GameObject.Find("Managers").GetComponent<ObjectSelector>();
        rm = GameObject.Find("Managers").GetComponent<ResourceManager>();
    }

    private void FixedUpdate()
    {
        if (ob.highlightedObj)
        {
            Transform ho;
            ho = ob.highlightedObj;
            display.text = ho.name;//shows tile name (which is set to it's type)
            
            display.text += "\nCurrent ore count: " + ho.GetComponent<TileInfo>().currentOreCount; //show ore count

            if (ho.GetComponent<TileInfo>().buildingChild)
            {
                bool energyCost = false;

                GameObject buildingChild = ho.GetComponent<TileInfo>().buildingChild;
                display.text += "\n---\n" + buildingChild.name; //shows building name

                BuildingInfo bInfo = buildingChild.GetComponent<BuildingInfo>();

                display.text += "\nCurrent Health: " + bInfo.currentHealth + "\nMax Health: " + bInfo.maxHealth; //health info

                display.text += "\nActive: " + bInfo.active + " (spacebar to toggle)"; //active state

                if (rm.energyTotal == 0) //shows powered stat - messy, maybe we should store a "lost all power" state
                    display.text += "\nPowered: False";
                else
                    display.text += "\nPowered: True";

                display.text += "\nCrew: " + bInfo.crew + " (up or down arrow to change)"; ; //crew count
                
                display.text += "\nSun Light: " + bInfo.sunLit + "\nElectric Light: " + bInfo.electicLit; //light info
                
                //energy section
                if (buildingChild.GetComponent<EnergyBuilding>())
                {
                    EnergyBuilding eb = buildingChild.GetComponent<EnergyBuilding>();
                    display.text += "\n---\nEnergy Type: " + eb.type;
                    display.text += "\nEnergy Gain: " + eb.energyGain + "(currently " + eb.energyGainCurrent + ")";
                }
                else
                {
                }

                if (buildingChild.GetComponent<BatteryBuilding>())
                {
                    BatteryBuilding bb = buildingChild.GetComponent<BatteryBuilding>();
                    display.text += "\nBattery Capacity: " + bb.capacity;
                }

                //mineral section
                if (buildingChild.GetComponent<MineralBuilding>())
                {
                    MineralBuilding mb = buildingChild.GetComponent<MineralBuilding>();
                    energyCost = true;
                    display.text += "\n---\nMineral Gain: " + mb.mineralGain + "(currently " + mb.mineralGainCurrent + ")";
                }

                //light section
                if (buildingChild.GetComponent<BuildingLight>())
                {
                    energyCost = true;
                }

                if (energyCost)
                    display.text += "\n---\nEnergy Cost: " + bInfo.energyCost + " (currently " + bInfo.energyCostCurrent + ")"; //if we cost energy,  display the cost
            }
        }
        else
        {
            display.text = "Objective: Place a harvester on each orange ore tile";
            display.text += "\n\nKeyboard Inputs\nBuildings";
            display.text += "\n1: build Solar (10 minerals)";
            display.text += "\n2: build Harvester (25 minerals)";
            display.text += "\n3: build Light (20 minerals)";
            display.text += "\n\nOther\nR: Reload scene";
            display.text += "\nX: Destroy building (can't yet)";
        }
    }
}
