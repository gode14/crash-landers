﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationManager : MonoBehaviour {

	//number of people (0 dead(removed),rest get own number for id purposes)
	public List<int> popList = new List<int> ();
	public int totalPop;
	public int currentPop;

	public int workers;

	void Start () {
		currentPop = 0;
		totalPop = 10;

    }
		
	void Update () {
		//if change in population size
		if (currentPop != totalPop)
			PopulationUpdater ();
	}
		
	//lets buildings add/remove workers
	public void AddWorkers(int workerChange){
        workers += workerChange;
        workers = Mathf.Clamp(workers, 0, currentPop);
	}
		
	//Update Population list if there is change in size
	void PopulationUpdater(){
		if(currentPop == 0)
		for (int i = 1; i <= totalPop; i++) {
			popList.Add (i);
		}
		popList.RemoveAll (i => i == 0);
		currentPop = totalPop;
		totalPop = popList.Count;
	}

}
