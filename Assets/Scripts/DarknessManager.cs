﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessManager : MonoBehaviour {

    public Texture electricLightCookie;
    Transform sun;
    ResourceManager rm;

    private void Awake()
    {
        sun = GameObject.Find("Sun").transform;
        rm = GetComponent<ResourceManager>();
    }

    //update darkness every tick
    int timer = 0;
    int refreshRate = 25;
    void FixedUpdate()
    {
        if (timer == 0)
        {
            CheckBuildings();
            timer = refreshRate;
        }
        else
            timer--;
    }

    void CheckBuildings()
    {
        foreach (var building in FindObjectsOfType<BuildingInfo>()) //each building
        {
            if(!CheckSunlight(building) && !CheckElectricLight(building))
            {
                building.ModifyHealth(-1);
            }
        }
    }

    //compare each buildings rotation to the rotation of the sun and tell each buildinginfo if we are sunLit or not (this seems quite cheap to process)
    //any script can call this, to get an instant light update
    public bool CheckSunlight(BuildingInfo building)
    {
        //calculate sunlight level
        int sunLightLevel = Mathf.RoundToInt((Quaternion.Angle(sun.rotation, building.transform.rotation) / 180));

        //right now sunlight is a bool so this is kinda messy. Maybe we will have more levels of sunlight in the future
        if (sunLightLevel == 1)
            building.sunLit = true;
        else
            building.sunLit = false;

        return building.sunLit;
    }

    //this is might be too expensive, could need a different solution like colliders
    bool CheckElectricLight(BuildingInfo building)
    {
        building.electicLit = false; //set eLit to false

        if (rm.energyTotal > 0) //if we have any energy, start check
        {
            foreach (var light in FindObjectsOfType<BuildingLight>()) //each light
            {
                //calculate distance from each light (static 35 rn because all lights are same radius)
                if (Vector3.Distance(light.transform.position, building.transform.position) < 16.5f)
                    building.electicLit = true;
            }
        }

        return building.electicLit;
    }
}
