﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    public int rotateSpeed;
    public Vector3 rotateVector;

    private void FixedUpdate()
    {
        transform.Rotate(rotateVector * rotateSpeed);
    }
}
