﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomer : MonoBehaviour {
    
    int zoomLevel = 1;
    Camera cam;

    Vector3 targetPos;
    float targetFoV;
    //offset and FoV size values for each of the zoom levels
    Vector3 zoomZeroPos = new Vector3(0, 0, -191);
    float zoomZeroFoV = 26.8f;
    Vector3 zoomOnePos = new Vector3(0, 25, -74);
    float zoomOneFoV = 52.2f;

    float timer = .4f;
    float time = 0;

    //this script creates two zoom levels with their own position and field of views and lerps between them according to scroll wheel input

    private void Awake()
    {
        cam = transform.Find("Camera").GetComponent<Camera>();
        targetPos = zoomZeroPos;
        targetFoV = zoomZeroFoV;
        time = timer;
    }

    float tracker;

    private void Update()
    {
        //update cooldown timer so you can't spam zoom-level change because it looked ugly if you changed it too often
        if (time >= timer)
        {
            ScrollWheel();
        }
        else
            time += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        tracker = EasingFunction.EaseInExpo(tracker, zoomLevel, 0.1f);

        cam.gameObject.transform.localPosition = Vector3.Lerp(cam.gameObject.transform.localPosition, targetPos, tracker);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetFoV, tracker);
    }

    void ScrollWheel()
    {
        float d = Input.GetAxis("Mouse ScrollWheel");

        if (d < 0f)
        {
            zoomLevel = 1;
            targetPos = zoomZeroPos;
            targetFoV = zoomZeroFoV;
            time = 0;
        }

        if (d > 0f)
        {
            zoomLevel = 0;
            targetPos = zoomOnePos;
            targetFoV = zoomOneFoV;
            time = 0;
        }
    }
}
